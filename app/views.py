from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms

from .forms import LoginForm, RegisterForm
from . import models

# Create your views here.
def check_login(request):
    if(request.session.get("username", False)):
        return True;
    return False;

def logout(request):
    try:
        del request.session['username']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('app:login'))

def login(request):
    if(request.method == 'GET'):
        if request.session.get("username", False):
            return HttpResponseRedirect(reverse('app:home', kwargs={'username' : request.session.get("username", False)}))
        form = LoginForm()
    else:
        form = LoginForm(request.POST)
        if form.is_valid():
            request.session['username'] = form.cleaned_data.get("username")
            return HttpResponseRedirect(reverse('app:home', kwargs={'username' : form.cleaned_data.get("username")}))

    return render(request, 'app/public/login.html', { 'form' : form })


def register(request):
    if(request.method == 'GET'):
        form = RegisterForm()
    else:
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = models.User.objects.create(username=form.cleaned_data.get("username"), password=form.cleaned_data.get("password"), first_name=form.cleaned_data.get("first_name"),
                                              last_name=form.cleaned_data.get("last_name"), email=form.cleaned_data.get("email"), score=1000, role="memeber")
            request.session['username'] = form.cleaned_data.get("username")
            return HttpResponseRedirect(reverse('app:home', kwargs={'username' : form.cleaned_data.get("username")}))

    return render(request, 'app/public/register.html', { 'form' : form })


def home(request, username):
    if (request.method == 'GET'):
        if not check_login(request):
            return HttpResponseRedirect(reverse('app:login'))
        try:
            user = models.User.objects.get(username=username)
        except models.User.DoesNotExist:
            raise Http404("<h1>404 Error: User does not exists</h1>")
        quizes = models.Quiz.objects.exclude(users=user)
        projects = models.Project.objects.exclude(users=user)
        questions = models.Question.objects.all()
        return render(request, 'app/user/home.html', { 'user': user, 'quizes' : quizes, 'projects': projects, 'questions': questions })

def getProject(request, id):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    user = models.User.objects.get(username=request.session.get('username'))
    try:
        project = models.Project.objects.get(id=id)
    except models.Project.DoesNotExist:
        raise Http404("<h1>404 Error: Project does not exists</h1>")

    if(user.score > project.min_score):
        project.users.add(user)
        project.save()
        user.score += project.score
        user.save()

    return HttpResponseRedirect(reverse('app:home', kwargs={ 'username' : user.username }))

def askQuestion(request):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    if(request.method == "POST"):
        user = models.User.objects.get(username=request.session['username'])
        models.Question.objects.create(title=request.POST['title'], description=request.POST['description'], user=user)
        return HttpResponseRedirect(reverse('app:home', kwargs={ 'username' : user.username }))

def showQuestion(request, id):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    question = models.Question.objects.get(id=id)
    user = models.User.objects.get(username=request.session['username'])
    if(request.method == "GET"):
        quizes = models.Quiz.objects.exclude(users=user)
        projects = models.Project.objects.exclude(users=user)
        answers = models.Answer.objects.filter(question=question)
        return render(request, 'app/user/show_problem.html', { 'user' : user, 'question' : question, 'answers' : answers , 'quizes' : quizes, 'projects' : projects });
    if(request.method == "POST"):
        models.Answer.objects.create(description=request.POST['description'], question=question, user=user)
        return HttpResponseRedirect(reverse('app:show_problem', kwargs={ 'id': question.id }))

def answerCorrectness(request, question_id, answer_id, user_id, is_correct):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    answer = models.Answer.objects.get(id=answer_id)
    answer.status = True
    answer.save()
    if(is_correct == 0):
        return HttpResponseRedirect(reverse('app:show_problem', kwargs={ 'id': question_id }))
    else:
        user = models.User.objects.get(id=user_id)
        user.score += 100
        user.save()
        return HttpResponseRedirect(reverse('app:show_problem', kwargs={ 'id': question_id }))

def makePorject(request):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    title = request.POST['title']
    description = request.POST['description']
    score = request.POST['score']
    min_score = request.POST['min_score']

    models.Project.objects.create(title=title, description=description, score=score, min_score=min_score)

    return HttpResponseRedirect(request.META['HTTP_REFERER'])


def makeQuiz(request):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    title = request.POST['title']
    if(models.Quiz.objects.filter(title=title).exists()):
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    quiz = models.Quiz.objects.create(title=title)
    return HttpResponseRedirect(reverse('app:make_quiz_question', kwargs={ 'quiz_id' : quiz.id }))

def makeQuizQuestion(request, quiz_id):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    if(request.method == "GET"):
        user = models.User.objects.get(username=request.session['username'])
        return render(request, 'app/user/quiz_question.html', { 'user' : user, 'quiz_id' : quiz_id })
    if(request.method == "POST"):
        title = request.POST['title']
        description = request.POST['description']
        difficaulty = request.POST['difficaulty']
        score = request.POST['score']
        quiz = models.Quiz.objects.get(pk=quiz_id)
        quiz_question = models.QuizQuestion.objects.create(quiz=quiz, title=title, description=description, dificulty=difficaulty, score=score)
        return  HttpResponseRedirect(reverse('app:make_quiz_question_choice', kwargs={ 'quiz_id': quiz_id, 'quiz_question_id' : quiz_question.id }))

def makeQuizQuestionChoice(request, quiz_id, quiz_question_id):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    user = models.User.objects.get(username=request.session['username'])
    if(request.method == "GET"):
        return render(request, 'app/user/quiz_question_choice.html', { 'user' : user, 'quiz_id' : quiz_id, 'quiz_question_id' : quiz_question_id })
    if(request.method == "POST"):
        quiz_question = models.QuizQuestion.objects.get(pk=quiz_question_id)
        choice1 = models.Choice.objects.create(text=request.POST['title_1'], quiz_question=quiz_question)
        choice2 = models.Choice.objects.create(text=request.POST['title_2'], quiz_question=quiz_question)
        choice3 = models.Choice.objects.create(text=request.POST['title_3'], quiz_question=quiz_question)
        choice4 = models.Choice.objects.create(text=request.POST['title_4'], quiz_question=quiz_question)
        if(request.POST['right_choice'] == 1):
            quiz_question.right_choice = choice1
        elif(request.POST['right_choice'] == 2):
            quiz_question.right_choice = choice2
        elif(request.POST['right_choice'] == 3):
            quiz_question.right_choice = choice3
        else:
            quiz_question.right_choice = choice4
        quiz_question.save()
        if(request.POST['submit'] == "submit"):
            return HttpResponseRedirect(reverse("app:home", kwargs={ "username" : user.username }))
        return HttpResponseRedirect(reverse('app:make_quiz_question', kwargs={ 'quiz_id' : quiz_id }))

def showQuiz(request, quiz_id):
    if not check_login(request):
        return HttpResponseRedirect(reverse('app:login'))
    user = models.User.objects.get(username=request.session['username'])
    if(request.method == "GET"):
        quiz = models.Quiz.objects.get(pk=quiz_id)
        if(not user in quiz.users.all()):
            quiz.users.add(user)
        quizes = models.Quiz.objects.exclude(users=user)
        projects = models.Project.objects.exclude(users=user)
        questions = models.QuizQuestion.objects.filter(quiz=quiz).order_by('?')[:10]
        return render(request, 'app/user/show_quiz.html', { 'user' : user, 'quiz' : quiz, 'projects' : projects, 'quizes' : quizes, 'questions' : questions })
    if(request.method == "POST"):
        score = 0;
        for i in range(1, 11):
            temp = request.POST[str(i)].split('-')
            q_id = temp[0]
            c_id = temp[1]
            question = models.QuizQuestion.objects.get(pk=q_id)
            if(question.right_choice == models.Choice.objects.get(pk=c_id)):
                score += question.score
        user.score += score
        user.save()
        return HttpResponseRedirect(reverse('app:home', kwargs={ 'username' : user.username }))
