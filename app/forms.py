from django import forms
from .models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder': 'username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'password'}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()

        if not User.objects.filter(username=cleaned_data.get("username"), password=cleaned_data.get("password")).exists():
            raise forms.ValidationError("username or password is invalid")

class RegisterForm(forms.Form):
    first_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder': 'first name'}))
    last_name = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder': 'last name'}))
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder': 'username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'password'}))
    confirm_password=forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'confirm password'}))
    email = forms.EmailField(help_text='A valid email address please.', widget=forms.TextInput(attrs={'placeholder': 'email'}))

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )

        if User.objects.filter(username=cleaned_data.get("username")).exists():
            raise forms.ValidationError(u'Username "%s" is already in use.' % cleaned_data.get("username"))
