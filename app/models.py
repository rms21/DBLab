from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    score = models.PositiveSmallIntegerField(null=True, blank=True)
    role = models.CharField(max_length=50, null=True, blank=True)

class Project(models.Model):
    title = models.CharField(max_length=254)
    description = models.TextField()
    score = models.PositiveSmallIntegerField()
    min_score = models.PositiveSmallIntegerField()
    users = models.ManyToManyField(User, null=True, blank=True)

class Quiz(models.Model):
    title = models.CharField(max_length=254)
    users = models.ManyToManyField(User, null=True, blank=True)

class QuizQuestion(models.Model):
    title = models.CharField(max_length=254)
    description = models.TextField()
    dificulty = models.CharField(max_length=1)
    score = models.PositiveSmallIntegerField()
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, null=True, blank=True)
    right_choice = models.OneToOneField('Choice', on_delete=models.CASCADE, null=True, blank=True)

    def choices(self):
        return self.choice_set.all()
        
class Choice(models.Model):
    text = models.CharField(max_length=254)
    quiz_question = models.ForeignKey(QuizQuestion, on_delete=models.CASCADE, null=True, blank=True)


class Question(models.Model):
    title = models.CharField(max_length=254)
    description = models.TextField()

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

class Answer(models.Model):
    description = models.TextField()
    status = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
