from django.conf.urls import url

from . import views

app_name = 'app'
urlpatterns = [
    url(r'^$', views.login, name='login'),
    url(r'^login/$', views.login, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^(?P<username>\w{0,50})/$', views.home, name='home'),
    url(r'^get/project/(?P<id>\d+)/$', views.getProject, name='get_project'),
    url(r'^question/ask/$', views.askQuestion, name='ask_question'),
    url(r'^problem/(?P<id>\d+)/$', views.showQuestion, name='show_problem'),
    url(r'^answer/correctness/(?P<question_id>\d+)/(?P<answer_id>\d+)/(?P<user_id>\d+)/(?P<is_correct>\d+)/$', views.answerCorrectness, name='answer_correctness'),
    url(r'^make/project/$', views.makePorject, name='make_project'),
    url(r'^make/quiz/$', views.makeQuiz, name='make_quiz'),
    url(r'^make/quiz/question/(?P<quiz_id>\d+)/$', views.makeQuizQuestion, name='make_quiz_question'),
    url(r'^make/quiz/question/choice/(?P<quiz_id>\d+)/(?P<quiz_question_id>\d+)/$', views.makeQuizQuestionChoice, name='make_quiz_question_choice'),
    url(r'^quiz/(?P<quiz_id>\d+)/$', views.showQuiz, name='show_quiz'),

]
