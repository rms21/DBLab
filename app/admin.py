from django.contrib import admin

from .models import User, Project, Question, Quiz, QuizQuestion, Choice, Answer
# Register your models here.
admin.site.register(User)
admin.site.register(Project)
admin.site.register(Quiz)
admin.site.register(QuizQuestion)
admin.site.register(Choice)
admin.site.register(Question)
admin.site.register(Answer)
